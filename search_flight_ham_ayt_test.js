Feature('Search');

Scenario('Suche nach Flug für eine Person von Hamburg nach Antalya',  ({ I }) => {
    I.amOnPage('https://flug.check24.de/');

    I.click("Geht klar"); // Cookie Banner wegklicken

    I.fillField("von", "HAM");
    I.click("Hamburg", { css: '[data-testid="airportItem"]' });

    I.fillField("nach", "AYT");
    I.click('Antalya', { css: '[data-testid="airportItem"]' });

    I.click("suche");
});
